package ohrm.tests;

import ohrm.utils.Steps;
import ohrm.utils.TestTemplate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;

import static ohrm.utils.TestCommons.*;
import static org.junit.jupiter.api.Assertions.*;


public class OrangeHRMTest extends TestTemplate {

    /*@BeforeAll
    public void setUp() {
        set();
    }

     */

    @AfterAll
    public void tearDown() {
        driver.quit();
    }

    @BeforeEach
    public void startAgain() {
        set();
    }

    @AfterEach
    public void closeWindow() {
        log.info("Close window");
        driver.close();
    }

    @Test
    @DisplayName("Login is successful with correct User and Password")
    public void loginSuccessful() {
        log.info("Login");
        Steps.login(driver, wait, js, USER, PASSWORD);
        assertFalse(driver.getTitle().equals(TITLE_HOME));
    }

    @Test
    @DisplayName("Login is unsuccessful with incorrect User and correct Password")
    public void loginFailsIncorrectUser() {
        Steps.login(driver, wait, js, WRONGUSER, PASSWORD);
        assertTrue(driver.getCurrentUrl().equals(BASE_URL));
    }

    @Test
    @DisplayName("Login is unsuccessful with correct User and incorrect Password")
    public void loginFailsIncorrectPassword() {
        Steps.login(driver, wait, js, USER, WRONGPASSWORD);
        assertTrue(driver.getCurrentUrl().equals(BASE_URL));
    }


}
