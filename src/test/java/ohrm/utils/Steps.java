package ohrm.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static ohrm.utils.TestCommons.*;

public class Steps {
    public static void login(WebDriver driver, WebDriverWait wait, JavascriptExecutor js, String USER, String PASSWORD) {
        driver.get(BASE_URL);
        driver.manage().window().maximize();
        driver.findElement(By.id("txtUsername")).sendKeys(USER);
        driver.findElement(By.id("txtPassword")).click();
        driver.findElement(By.id("txtPassword")).sendKeys(PASSWORD);
        driver.findElement(By.id("btnLogin"));

    }
}
