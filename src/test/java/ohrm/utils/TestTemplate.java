package ohrm.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.message.EntryMessage;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFactory;
import org.apache.logging.log4j.util.MessageSupplier;
import org.apache.logging.log4j.util.Supplier;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class TestTemplate {
    protected static WebDriver driver;
    protected static JavascriptExecutor js;
    protected static WebDriverWait wait;
    protected static Logger log = LogManager.getLogger(TestTemplate.class);

    public void set() {
        WebDriverManager.chromedriver().setup();
        driver= new ChromeDriver();
        js = (JavascriptExecutor) driver;
        wait = new WebDriverWait(driver, 30);
        log = new Logger() {
            public void catching(Level level, Throwable throwable) {

            }

            public void catching(Throwable throwable) {

            }

            public void debug(Marker marker, Message message) {

            }

            public void debug(Marker marker, Message message, Throwable throwable) {

            }

            public void debug(Marker marker, MessageSupplier messageSupplier) {

            }

            public void debug(Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void debug(Marker marker, CharSequence charSequence) {

            }

            public void debug(Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void debug(Marker marker, Object o) {

            }

            public void debug(Marker marker, Object o, Throwable throwable) {

            }

            public void debug(Marker marker, String s) {

            }

            public void debug(Marker marker, String s, Object... objects) {

            }

            public void debug(Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void debug(Marker marker, String s, Throwable throwable) {

            }

            public void debug(Marker marker, Supplier<?> supplier) {

            }

            public void debug(Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void debug(Message message) {

            }

            public void debug(Message message, Throwable throwable) {

            }

            public void debug(MessageSupplier messageSupplier) {

            }

            public void debug(MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void debug(CharSequence charSequence) {

            }

            public void debug(CharSequence charSequence, Throwable throwable) {

            }

            public void debug(Object o) {

            }

            public void debug(Object o, Throwable throwable) {

            }

            public void debug(String s) {

            }

            public void debug(String s, Object... objects) {

            }

            public void debug(String s, Supplier<?>... suppliers) {

            }

            public void debug(String s, Throwable throwable) {

            }

            public void debug(Supplier<?> supplier) {

            }

            public void debug(Supplier<?> supplier, Throwable throwable) {

            }

            public void debug(Marker marker, String s, Object o) {

            }

            public void debug(Marker marker, String s, Object o, Object o1) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void debug(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void debug(String s, Object o) {

            }

            public void debug(String s, Object o, Object o1) {

            }

            public void debug(String s, Object o, Object o1, Object o2) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void debug(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void entry() {

            }

            public void entry(Object... objects) {

            }

            public void error(Marker marker, Message message) {

            }

            public void error(Marker marker, Message message, Throwable throwable) {

            }

            public void error(Marker marker, MessageSupplier messageSupplier) {

            }

            public void error(Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void error(Marker marker, CharSequence charSequence) {

            }

            public void error(Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void error(Marker marker, Object o) {

            }

            public void error(Marker marker, Object o, Throwable throwable) {

            }

            public void error(Marker marker, String s) {

            }

            public void error(Marker marker, String s, Object... objects) {

            }

            public void error(Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void error(Marker marker, String s, Throwable throwable) {

            }

            public void error(Marker marker, Supplier<?> supplier) {

            }

            public void error(Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void error(Message message) {

            }

            public void error(Message message, Throwable throwable) {

            }

            public void error(MessageSupplier messageSupplier) {

            }

            public void error(MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void error(CharSequence charSequence) {

            }

            public void error(CharSequence charSequence, Throwable throwable) {

            }

            public void error(Object o) {

            }

            public void error(Object o, Throwable throwable) {

            }

            public void error(String s) {

            }

            public void error(String s, Object... objects) {

            }

            public void error(String s, Supplier<?>... suppliers) {

            }

            public void error(String s, Throwable throwable) {

            }

            public void error(Supplier<?> supplier) {

            }

            public void error(Supplier<?> supplier, Throwable throwable) {

            }

            public void error(Marker marker, String s, Object o) {

            }

            public void error(Marker marker, String s, Object o, Object o1) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void error(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void error(String s, Object o) {

            }

            public void error(String s, Object o, Object o1) {

            }

            public void error(String s, Object o, Object o1, Object o2) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void error(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void exit() {

            }

            public <R> R exit(R r) {
                return null;
            }

            public void fatal(Marker marker, Message message) {

            }

            public void fatal(Marker marker, Message message, Throwable throwable) {

            }

            public void fatal(Marker marker, MessageSupplier messageSupplier) {

            }

            public void fatal(Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void fatal(Marker marker, CharSequence charSequence) {

            }

            public void fatal(Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void fatal(Marker marker, Object o) {

            }

            public void fatal(Marker marker, Object o, Throwable throwable) {

            }

            public void fatal(Marker marker, String s) {

            }

            public void fatal(Marker marker, String s, Object... objects) {

            }

            public void fatal(Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void fatal(Marker marker, String s, Throwable throwable) {

            }

            public void fatal(Marker marker, Supplier<?> supplier) {

            }

            public void fatal(Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void fatal(Message message) {

            }

            public void fatal(Message message, Throwable throwable) {

            }

            public void fatal(MessageSupplier messageSupplier) {

            }

            public void fatal(MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void fatal(CharSequence charSequence) {

            }

            public void fatal(CharSequence charSequence, Throwable throwable) {

            }

            public void fatal(Object o) {

            }

            public void fatal(Object o, Throwable throwable) {

            }

            public void fatal(String s) {

            }

            public void fatal(String s, Object... objects) {

            }

            public void fatal(String s, Supplier<?>... suppliers) {

            }

            public void fatal(String s, Throwable throwable) {

            }

            public void fatal(Supplier<?> supplier) {

            }

            public void fatal(Supplier<?> supplier, Throwable throwable) {

            }

            public void fatal(Marker marker, String s, Object o) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void fatal(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void fatal(String s, Object o) {

            }

            public void fatal(String s, Object o, Object o1) {

            }

            public void fatal(String s, Object o, Object o1, Object o2) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void fatal(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public Level getLevel() {
                return null;
            }

            public <MF extends MessageFactory> MF getMessageFactory() {
                return null;
            }

            public String getName() {
                return null;
            }

            public void info(Marker marker, Message message) {

            }

            public void info(Marker marker, Message message, Throwable throwable) {

            }

            public void info(Marker marker, MessageSupplier messageSupplier) {

            }

            public void info(Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void info(Marker marker, CharSequence charSequence) {

            }

            public void info(Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void info(Marker marker, Object o) {

            }

            public void info(Marker marker, Object o, Throwable throwable) {

            }

            public void info(Marker marker, String s) {

            }

            public void info(Marker marker, String s, Object... objects) {

            }

            public void info(Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void info(Marker marker, String s, Throwable throwable) {

            }

            public void info(Marker marker, Supplier<?> supplier) {

            }

            public void info(Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void info(Message message) {

            }

            public void info(Message message, Throwable throwable) {

            }

            public void info(MessageSupplier messageSupplier) {

            }

            public void info(MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void info(CharSequence charSequence) {

            }

            public void info(CharSequence charSequence, Throwable throwable) {

            }

            public void info(Object o) {

            }

            public void info(Object o, Throwable throwable) {

            }

            public void info(String s) {

            }

            public void info(String s, Object... objects) {

            }

            public void info(String s, Supplier<?>... suppliers) {

            }

            public void info(String s, Throwable throwable) {

            }

            public void info(Supplier<?> supplier) {

            }

            public void info(Supplier<?> supplier, Throwable throwable) {

            }

            public void info(Marker marker, String s, Object o) {

            }

            public void info(Marker marker, String s, Object o, Object o1) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void info(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void info(String s, Object o) {

            }

            public void info(String s, Object o, Object o1) {

            }

            public void info(String s, Object o, Object o1, Object o2) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void info(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public boolean isDebugEnabled() {
                return false;
            }

            public boolean isDebugEnabled(Marker marker) {
                return false;
            }

            public boolean isEnabled(Level level) {
                return false;
            }

            public boolean isEnabled(Level level, Marker marker) {
                return false;
            }

            public boolean isErrorEnabled() {
                return false;
            }

            public boolean isErrorEnabled(Marker marker) {
                return false;
            }

            public boolean isFatalEnabled() {
                return false;
            }

            public boolean isFatalEnabled(Marker marker) {
                return false;
            }

            public boolean isInfoEnabled() {
                return false;
            }

            public boolean isInfoEnabled(Marker marker) {
                return false;
            }

            public boolean isTraceEnabled() {
                return false;
            }

            public boolean isTraceEnabled(Marker marker) {
                return false;
            }

            public boolean isWarnEnabled() {
                return false;
            }

            public boolean isWarnEnabled(Marker marker) {
                return false;
            }

            public void log(Level level, Marker marker, Message message) {

            }

            public void log(Level level, Marker marker, Message message, Throwable throwable) {

            }

            public void log(Level level, Marker marker, MessageSupplier messageSupplier) {

            }

            public void log(Level level, Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void log(Level level, Marker marker, CharSequence charSequence) {

            }

            public void log(Level level, Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void log(Level level, Marker marker, Object o) {

            }

            public void log(Level level, Marker marker, Object o, Throwable throwable) {

            }

            public void log(Level level, Marker marker, String s) {

            }

            public void log(Level level, Marker marker, String s, Object... objects) {

            }

            public void log(Level level, Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void log(Level level, Marker marker, String s, Throwable throwable) {

            }

            public void log(Level level, Marker marker, Supplier<?> supplier) {

            }

            public void log(Level level, Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void log(Level level, Message message) {

            }

            public void log(Level level, Message message, Throwable throwable) {

            }

            public void log(Level level, MessageSupplier messageSupplier) {

            }

            public void log(Level level, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void log(Level level, CharSequence charSequence) {

            }

            public void log(Level level, CharSequence charSequence, Throwable throwable) {

            }

            public void log(Level level, Object o) {

            }

            public void log(Level level, Object o, Throwable throwable) {

            }

            public void log(Level level, String s) {

            }

            public void log(Level level, String s, Object... objects) {

            }

            public void log(Level level, String s, Supplier<?>... suppliers) {

            }

            public void log(Level level, String s, Throwable throwable) {

            }

            public void log(Level level, Supplier<?> supplier) {

            }

            public void log(Level level, Supplier<?> supplier, Throwable throwable) {

            }

            public void log(Level level, Marker marker, String s, Object o) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void log(Level level, Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void log(Level level, String s, Object o) {

            }

            public void log(Level level, String s, Object o, Object o1) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void log(Level level, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void printf(Level level, Marker marker, String s, Object... objects) {

            }

            public void printf(Level level, String s, Object... objects) {

            }

            public <T extends Throwable> T throwing(Level level, T t) {
                return null;
            }

            public <T extends Throwable> T throwing(T t) {
                return null;
            }

            public void trace(Marker marker, Message message) {

            }

            public void trace(Marker marker, Message message, Throwable throwable) {

            }

            public void trace(Marker marker, MessageSupplier messageSupplier) {

            }

            public void trace(Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void trace(Marker marker, CharSequence charSequence) {

            }

            public void trace(Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void trace(Marker marker, Object o) {

            }

            public void trace(Marker marker, Object o, Throwable throwable) {

            }

            public void trace(Marker marker, String s) {

            }

            public void trace(Marker marker, String s, Object... objects) {

            }

            public void trace(Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void trace(Marker marker, String s, Throwable throwable) {

            }

            public void trace(Marker marker, Supplier<?> supplier) {

            }

            public void trace(Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void trace(Message message) {

            }

            public void trace(Message message, Throwable throwable) {

            }

            public void trace(MessageSupplier messageSupplier) {

            }

            public void trace(MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void trace(CharSequence charSequence) {

            }

            public void trace(CharSequence charSequence, Throwable throwable) {

            }

            public void trace(Object o) {

            }

            public void trace(Object o, Throwable throwable) {

            }

            public void trace(String s) {

            }

            public void trace(String s, Object... objects) {

            }

            public void trace(String s, Supplier<?>... suppliers) {

            }

            public void trace(String s, Throwable throwable) {

            }

            public void trace(Supplier<?> supplier) {

            }

            public void trace(Supplier<?> supplier, Throwable throwable) {

            }

            public void trace(Marker marker, String s, Object o) {

            }

            public void trace(Marker marker, String s, Object o, Object o1) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void trace(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void trace(String s, Object o) {

            }

            public void trace(String s, Object o, Object o1) {

            }

            public void trace(String s, Object o, Object o1, Object o2) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void trace(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public EntryMessage traceEntry() {
                return null;
            }

            public EntryMessage traceEntry(String s, Object... objects) {
                return null;
            }

            public EntryMessage traceEntry(Supplier<?>... suppliers) {
                return null;
            }

            public EntryMessage traceEntry(String s, Supplier<?>... suppliers) {
                return null;
            }

            public EntryMessage traceEntry(Message message) {
                return null;
            }

            public void traceExit() {

            }

            public <R> R traceExit(R r) {
                return null;
            }

            public <R> R traceExit(String s, R r) {
                return null;
            }

            public void traceExit(EntryMessage entryMessage) {

            }

            public <R> R traceExit(EntryMessage entryMessage, R r) {
                return null;
            }

            public <R> R traceExit(Message message, R r) {
                return null;
            }

            public void warn(Marker marker, Message message) {

            }

            public void warn(Marker marker, Message message, Throwable throwable) {

            }

            public void warn(Marker marker, MessageSupplier messageSupplier) {

            }

            public void warn(Marker marker, MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void warn(Marker marker, CharSequence charSequence) {

            }

            public void warn(Marker marker, CharSequence charSequence, Throwable throwable) {

            }

            public void warn(Marker marker, Object o) {

            }

            public void warn(Marker marker, Object o, Throwable throwable) {

            }

            public void warn(Marker marker, String s) {

            }

            public void warn(Marker marker, String s, Object... objects) {

            }

            public void warn(Marker marker, String s, Supplier<?>... suppliers) {

            }

            public void warn(Marker marker, String s, Throwable throwable) {

            }

            public void warn(Marker marker, Supplier<?> supplier) {

            }

            public void warn(Marker marker, Supplier<?> supplier, Throwable throwable) {

            }

            public void warn(Message message) {

            }

            public void warn(Message message, Throwable throwable) {

            }

            public void warn(MessageSupplier messageSupplier) {

            }

            public void warn(MessageSupplier messageSupplier, Throwable throwable) {

            }

            public void warn(CharSequence charSequence) {

            }

            public void warn(CharSequence charSequence, Throwable throwable) {

            }

            public void warn(Object o) {

            }

            public void warn(Object o, Throwable throwable) {

            }

            public void warn(String s) {

            }

            public void warn(String s, Object... objects) {

            }

            public void warn(String s, Supplier<?>... suppliers) {

            }

            public void warn(String s, Throwable throwable) {

            }

            public void warn(Supplier<?> supplier) {

            }

            public void warn(Supplier<?> supplier, Throwable throwable) {

            }

            public void warn(Marker marker, String s, Object o) {

            }

            public void warn(Marker marker, String s, Object o, Object o1) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void warn(Marker marker, String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }

            public void warn(String s, Object o) {

            }

            public void warn(String s, Object o, Object o1) {

            }

            public void warn(String s, Object o, Object o1, Object o2) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3, Object o4) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8) {

            }

            public void warn(String s, Object o, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6, Object o7, Object o8, Object o9) {

            }
        };
    }
}
