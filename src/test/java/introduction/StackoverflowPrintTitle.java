package introduction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.Assert.assertEquals; // to add assertEqual function

public class StackoverflowPrintTitle {
    private WebDriver driver;

    @Before //
    public void setUp() {
        driver = new ChromeDriver(); // ChromeDriver initialization
    }

    @Test
    public void StackoverflowPrintTitleToConsole() {

        driver.get("https://stackoverflow.com/"); //open stackoverflow.com

        /*
        if (driver.getTitle().contains("Stack Overflow - Where Developers Learn, Share, & Build Careers")) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
        System.out.println(driver.getTitle()); // retrieve page's title and print it to the console
         */

        assertEquals("Stack Overflow - Where Developers Learn, Share, & Build Careers", driver.getTitle());
        //checking if the current page title is expected

    }



    @After
    public void tearDown() {
        driver.close(); //close browser
    }
}
