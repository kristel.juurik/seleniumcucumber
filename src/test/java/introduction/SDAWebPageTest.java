package introduction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SDAWebPageTest {
    private WebDriver driver;

    @Before //
    public void setUp() {
        driver = new ChromeDriver(); // ChromeDriver initialization
    }

    @Test
    public void SDAWebPageTestPrintTitleToConsole() {

        driver.get("https://sdacademy.dev"); //open https://sdacademy.dev

        System.out.println(driver.getTitle()); // retrieve page's title and print it to the console

    }

    @After
    public void tearDown() {
        driver.close(); //close browser
    }
}
