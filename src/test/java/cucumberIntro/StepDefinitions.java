package cucumberIntro;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import ohrm.utils.TestTemplate;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static ohrm.utils.TestCommons.BASE_URL;
import static ohrm.utils.TestCommons.TITLE_HOME;
import static org.junit.Assert.*;

public class StepDefinitions extends TestTemplate{
    @Before
    public void setUp(){
        set();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Given("I am on login page")
    public void openHomePage() {
        driver.get(BASE_URL);
        driver.manage().window().maximize();
    }

    @When("I enter {string} in the \"USERNAME\" field")
    public void insertUser(String USER) {
        driver.findElement(By.cssSelector("#divUsername > .form-hint")).click();
        driver.findElement(By.id("txtUsername")).sendKeys(USER);
    }

    @When("I enter {string} in the \"PASSWORD\" field")
    public void insertPassword(String PASSWORD) {
        driver.findElement(By.id("txtPassword")).click();
        driver.findElement(By.id("txtPassword")).sendKeys(PASSWORD);
    }

    @When("I click on the \"Login\" button")
    public void submitLoginForm()  {
        driver.findElement(By.id("btnLogin")).click();
    }

    @Then("I am on home page")
    public void checkHomePage() {
        wait.until(ExpectedConditions.titleIs(TITLE_HOME));
        assertTrue(driver.getTitle().equals(TITLE_HOME));
    }
}
